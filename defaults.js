var defaults = {};

defaults.db = "mongodb://localhost:27017/test";

defaults.collections = {};
defaults.plugins = {};

defaults.collections.objects = {
	collection: "objects",
	attributesFetch: ['object', 'modified', 'created', 'version'],
	attributesDiff: ['object', 'permissions']
};

/*
defaults.plugins.objects = {
	file: './OmsObjects.js',
	config: {}
};
*/

defaults.plugins.messenger = {
	file: './OmsMessenger.js',
	config: {
		collection: 'messenger',
		size: 5242880, // size of capped collection, in bytes
		max: 10 // max number of messages in capped collection (minimum 2)
	}
};

defaults.plugins.subscriptions = {
	file: './OmsSubscriptions.js',
	config: {}
};

defaults.plugins.permissions = {
	file: './OmsPermissions.js',
	config: {}
};

module.exports = defaults;
var assert = require('assert');
var oms = require('../')(); // use defaults (localhost/test) as defined in defaults.js

describe('OMS Doc Subscribe', function(){
	describe('Create, subscribe and trigger an event', function(){
		it('should create an doc, subscribe to a custom event, then trigger the custom event', function(done) {
			oms.docInsert({object: "someval1" }, function(error, objectExtended) {
				if(error)
					throw error;
				if(typeof objectExtended != "object" || typeof objectExtended._id == "undefined")
					throw new Error("Invalid object returned by insert");

				var id = objectExtended._id;

				var actionArg = {myvar: "myval"};

				function subscriptionTriggered(arg) {
					if (arg == actionArg)
						done();
					else
						throw new Error("Arguments don't match");
				}

				oms.subscriptionAdd(id, 'mycustomevent', subscriptionTriggered);

				oms.subscriptionTrigger(id, 'mycustomevent', actionArg);

			});
		});
	});
});
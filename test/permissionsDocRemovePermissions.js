var assert = require('assert');
var oms = require('../')(); // use defaults (localhost/test) as defined in defaults.js

describe('OMS Doc Permissions Add', function(){
	describe('Create and update', function(){
		it('should create a doc, and remove action permissions for a user', function(done) {
			oms.permissionsDocInsert('userid', {object: "someval1" }, function(error, objectExtended) {
				if(error)
					throw error;
				if(typeof objectExtended != "object" || typeof objectExtended._id == "undefined")
					throw new Error("Invalid object returned by insert");

				var id = objectExtended._id;

				oms.permissionsDocAddPermissions('userid', 'userid', 'testaction', id, function(error, docDiffs) {
					if(error)
						throw error;
					else {
						oms.permissionsDocRemovePermissions('userid', 'userid', 'testaction', id, function(error, docDiffs) {
							//console.log(docDiffs);
							done();
						});
					}
				});
			});
		});
	});
});
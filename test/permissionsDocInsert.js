var assert = require('assert');
var oms = require('../')(); // use defaults (localhost/test) as defined in defaults.js

describe('OMS Doc Insertion with permissions', function(){
	describe('Regular doc', function(){
		it('should return an _id when doc is inserted wih permissions', function(done) {
			oms.permissionsDocInsert('userid', {key: "someval1" }, function(error, object) {
				if(error)
					throw error;
				else {
					var id = object._id;
					oms.docFind({_id: id}, function(error, results) {
						if(error)
							throw error;
						if(!results || results.length != 1)
							throw new Error("inserted object '" + id + "' not found in DB");
						done();
					});
				}
			});
		});
	});
});
var assert = require('assert');
var oms = require('../')(); // use defaults (localhost/test) as defined in defaults.js
var jsondiffpatch = require('jsondiffpatch');

describe('OMS Doc Subscribe', function(){
	describe('Create, update and subscribe', function(){
		it('should create an doc, subscribe to updates on it, and update it', function(done) {
			oms.docInsert({object: "someval1" }, function(error, objectExtended) {
				if(error)
					throw error;
				if(typeof objectExtended != "object" || typeof objectExtended._id == "undefined")
					throw new Error("Invalid object returned by insert");

				var id = objectExtended._id;

				function subscriptionTriggered(args) {
					//console.log(jsondiffpatch.patch(objectExtended, args.docDiff));
					done();
				}

				oms.subscriptionAdd(id, 'update', subscriptionTriggered);

				oms.docUpdate({object: "someval22"}, id,function(error, objDiff) {
					if(error)
						throw error;
				});
			});
		});
	});
});
var fauxmongo = require('fauxmongo');
var Utils = require('./Utils');

function mongoUpdatePatch(doc, update, clone) {

	if(clone)
		doc = objectClone(doc);

	function objectClone(object) {
		return JSON.parse(JSON.stringify(object));
	}

	var updateMongoOperators = {};

	Utils.objectForEach(update, function(operationValue, operationKey) {
		if(operationKey.charAt(0) == '$') // operation
			updateMongoOperators[operationKey] = operationValue;
		else
			doc[operationKey] = operationValue;
	});

	fauxmongo.update(doc, updateMongoOperators);

	return doc;
}

module.exports = mongoUpdatePatch;
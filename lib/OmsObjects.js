var Utils = require('./Utils');
var npm = require('npm');

function OmsObjects(pluginConfig, oms) {
	this.config = pluginConfig;
	this.oms = oms;
	this._attachGlobalFunctions();

	/*this._installNpmModule('express@4.x.x', function(error, installDir) {
		console.log(error, installDir);

	})*/
}

OmsObjects.prototype.objectInsertClassId = function(object, classId, callback) {
	// TODO validate that classId exists
	// TODO add option for extra metadata
	this.oms.docInsert({classId: classId, object: object}, callback);
};

/*
OmsObjects.prototype.objectUpdateSingle = function(objectUpdateAttributes, objectFilter, callback) {
	if(typeof objectFilter == 'object') {
		var docFilter = {};
		Utils.objectForEach(objectFilter, function(attributeValue, attributeKey) {
			docFilter['object.' + attributeKey] = attributeValue;
		});
	}
	else
		docFilter = objectFilter;

	var docUpdate = {};
	Utils.objectForEach(objectUpdateAttributes, function(attributeValue, attributeKey) {
		docUpdate['object.' + attributeKey] = attributeValue;
	});

	this.oms.docUpdate(docFilter, docFilter, function(error, docDiff) {

	});
};
*/

OmsObjects.prototype.classInsert = function(className, callback) {
	// TODO Find class if it exists
	this.oms.docInsert({className: className}, callback);
};

OmsObjects.prototype._attachGlobalFunctions = function() {
	var self = this;
	self._globalFunctions().forEach(function(functionName) {
		self.oms[functionName] = function() {
			self[functionName].apply(self, arguments);
		};
	});
};

OmsObjects.prototype._globalFunctions = function() {
	function objFunctions(object) {
		var functionNames = [];
		for(var attribute in object) {
			if(typeof object[attribute] === 'function')
				functionNames.push(attribute);
		}
		return functionNames;
	}

	return objFunctions(this).filter(function(attribute) {
		return (attribute[0] !== '_' && attribute !== 'globalFunctions');
	});
};


OmsObjects.prototype._installNpmModule = function() {
	var npmPkg = arguments[0];
	if(arguments.length === 2 && typeof arguments[1] === "function") {// npmPkg, callback
		var callback = arguments[1];
		var basedir = ".";
	}
	else if(arguments.length === 3 && typeof arguments[1] === "string" && typeof arguments[2] === "function") {// npmPkg, basedir, callback
		var basedir = arguments[1];
		var callback = arguments[2];
	}
	else
		throw new Error("invalid_args");

	npm.load({
			loaded: false
		},
		function (err) {
			if(err)
				callback(err, null);
			else {
				try {
					npm.commands.install(basedir, [npmPkg], function (err, data) {

						if (err)
							callback(err, null);
						else {
							// TODO add error checking
							var moduleDir = data[data.length - 1][1];
							var defaultInstallDir = 'node_modules/';
							if(moduleDir.indexOf(defaultInstallDir) == 0)
								moduleDir = moduleDir.substr(defaultInstallDir.length);
							callback(null, moduleDir);
						}
					});
				}
				catch(error) {
					console.log(error);
					callback(error, null);
				}
			}
		});
};

module.exports = OmsObjects;
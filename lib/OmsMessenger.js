var Utils = require('./Utils');

function OmsMessenger(pluginConfig, oms) {
	this.nodeId = Utils.uniqueId();
	this.oms = oms;
	this.config = pluginConfig;
	this._attachGlobalFunctions();
}

OmsMessenger.prototype._attachGlobalFunctions = function() {
	var self = this;

	function globalFunctionName(localFunctionName) {
		return 'messenger' + localFunctionName.charAt(0).toUpperCase() + localFunctionName.slice(1);
	}

	self._globalFunctions().forEach(function(functionName) {
		self.oms[globalFunctionName(functionName)] = function() {
			self[functionName].apply(self, arguments);
		};
	});
};

OmsMessenger.prototype._globalFunctions = function() {
	function objFunctions(object) {
		var functionNames = [];
		for(var attribute in object) {
			if(typeof object[attribute] === 'function')
				functionNames.push(attribute);
		}
		return functionNames;
	}

	return objFunctions(this).filter(function(attribute) {
		return (attribute[0] !== '_' && attribute !== 'globalFunctions');
	});
};

OmsMessenger.prototype.destroy = function(channel, callback) {
	// only to adhere to other pub/sub-type systems
	callback(null, true);
};

OmsMessenger.prototype.create = function(channel, callback) {
	var self = this;
	self.oms.omsCore.dbOperation('createCollection', [self.config.collection, {
		capped: true,
		size: self.config.size,
		max: self.config.max
	}, triggerCallback]);

	function triggerCallback(error, result) {
		if(typeof callback === "function")
			callback(error, result);
	}
};

OmsMessenger.prototype.subscribe = function(channel, messageCallback, subscribeCallback) {
	var self = this;
	// TODO add check that collection exists, is capped (can use channelCreate)
	self.publish(channel, 'subscribe', {}, function(error, messageId) {
		if(error)
			triggerCallback(error, messageId);
		else {
			var findOptions = {channel: channel};
			var cursorOptions = {
				tailable: true,
				awaitdata: true,
				numberOfRetries: Number.MAX_VALUE
			};

			self.oms.omsCore.collectionOperation(self.config.collection, 'find', [findOptions, cursorOptions], processStream);

			function processStream(error, stream) {
				if(error)
					triggerCallback(error, null);
				else {
					try {
						stream.stream();

						var reachedStartObj = false;

						stream.on('data', function (object) {
							if (!reachedStartObj) {
								if (String(object._id) == String(messageId)) {
									reachedStartObj = true;
									triggerCallback(null, true);
								}
							}
							else if (typeof object.type === "string" && object.type === "message" && object.nodeId != self.nodeId)
								messageCallback(null, object.message);
						});

						stream.on('error', function (error) {
							messageCallback(error, null);
						});

						stream.on('end', function () {
							messageCallback("cursor_end", null);
						});
					}
					catch(error) {
						triggerCallback(error, null);
					}
				}
			}
		}
	});

	function triggerCallback(error, result) {
		if(typeof subscribeCallback === "function")
			subscribeCallback(error, result);
	}
};

OmsMessenger.prototype.publish = function() {
	// channel, [msgType], object, callback
	var channel = arguments[0];
	var type = arguments.length == 4 ? arguments[1] : 'message';
	var object = arguments.length == 4 ? arguments[2] : arguments[1];
	var callback = arguments.length == 4 ? arguments[3] : arguments[2];

	var message = {channel: channel, type: type, message: object, nodeId: this.nodeId};

	var afterInsert = function(error) {
		if(typeof callback === 'function') {
			if (error)
				callback(error, null);
			else
				callback(null, message._id);
		}
	};

	this.oms.omsCore.collectionOperation(this.config.collection, 'insert', [message, afterInsert]);
};

module.exports = OmsMessenger;
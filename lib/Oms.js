var OmsCore = require('./OmsCore');
var Utils = require('./Utils');

function Oms(config) {
	this.omsCore = new OmsCore(config);
	this.loadPlugins(this.omsCore.config.plugins);
}

// Find a document in the objects table
// filters, [fields,] callback
Oms.prototype.docFind = function() {

	// Arguments: filters, [fields], callback
	var collArgs = Array.prototype.slice.call(arguments);
	var callback = collArgs.pop(); // callback not passed to plugins
	var self = this;

	// TODO always pass as object that can be modified
	var hookArg = Utils.parseArgs(
		collArgs,
		[
			{name: 'filter', level: 0},
			{name: 'fields', level: 1}
		]
	);

	Utils.callbackQueue(
		[preDocFind, docFind, postDocFind],
		function(error) {
			callback(error, null);
		},
		function() {
			callback(null, hookArg.result);
		}
	);

	function preDocFind(successCallback, errorCallback) {
		self._runPluginHooks('preDocFind', hookArg, function(error) {
			if(error)
				errorCallback(error);
			else
				successCallback();
		});
	}

	function docFind(successCallback, errorCallback) {
		if(typeof collArgs.fields != 'undefined')
			self.omsCore.collectionDocFind(self.omsCore.config.collections.objects, hookArg.filter, hookArg.fields, docFindReturn);
		else
			self.omsCore.collectionDocFind(self.omsCore.config.collections.objects, hookArg.filter, docFindReturn);

		function docFindReturn(error, result) {
			if(error)
				errorCallback(error);
			else {
				hookArg.result = result;
				successCallback();
			}
		}
	}

	function postDocFind(successCallback, errorCallback) {
		self._runPluginHooks('postDocFind', hookArg, function(error) {
			if(error)
				errorCallback(error);
			else
				successCallback();
		});
	}
};

// Insert a document into the objects table
// Note: This does not perform validation
Oms.prototype.docInsert = function(doc, callback) {
	var self = this;

	var hookArg = {
		doc: doc
	};

	Utils.callbackQueue(
		[preDocInsert, docInsert, postDocInsert],
		function(error) {
			callback(error, null);
		},
		function() {
			callback(null, hookArg.docInserted);
		}
	);

	function preDocInsert(successCallback, errorCallback) {
		self._runPluginHooks('preDocInsert', hookArg, function(error) {
			if(error)
				errorCallback(error);
			else
				successCallback();
		});
	}

	function docInsert(successCallback, errorCallback) {
		self.omsCore.collectionDocInsert(self.omsCore.config.collections.objects, hookArg.doc, function(error, docInserted) {
			if(error)
				errorCallback(error);
			else {
				hookArg.docInserted = docInserted;
				successCallback();
			}
		});
	}

	function postDocInsert(successCallback, errorCallback) {
		self._runPluginHooks('postDocInsert', hookArg, function(error) {
			if(error)
				errorCallback(error);
			else
				successCallback();
		});
	}
};

Oms.prototype.docDelete = function(filter, callback) {
	var self = this;

	var hookArg = {
		filter: filter
	};

	Utils.callbackQueue(
		[preDocDelete, docDelete, postDocDelete],
		function(error) {
			callback(error, null);
		},
		function() {
			callback(null, hookArg.deleted);
		}
	);

	function preDocDelete(successCallback, errorCallback) {
		self._runPluginHooks('preDocDelete', hookArg, function(error) {
			if(error)
				errorCallback(error);
			else
				successCallback();
		});
	}

	function docDelete(successCallback, errorCallback) {
		self.omsCore.collectionDocDelete(self.omsCore.config.collections.objects, hookArg.filter, function(error, numDeleted) {
			if(error)
				errorCallback(error);
			else {
				hookArg.deleted = numDeleted;
				successCallback();
			}
		});
	}

	function postDocDelete(successCallback, errorCallback) {
		self._runPluginHooks('postDocDelete', hookArg, function(error) {
			if(error)
				errorCallback(error);
			else
				successCallback();
		});
	}
};

Oms.prototype.docUpdate = function(updateAttributes, filter, callback) {
	var self = this;

	var hookArg = {
		filter: filter,
		updateAttributes: updateAttributes
	};

	Utils.callbackQueue(
		[preDocUpdate, docUpdate, postDocUpdate],
		function(error) {
			callback(error, null);
		},
		function() {
			callback(null, hookArg.docDiffs);
		}
	);

	function preDocUpdate(successCallback, errorCallback) {
		self._runPluginHooks('preDocUpdate', hookArg, function(error) {
			if(error)
				errorCallback(error);
			else
				successCallback();
		});
	}

	function docUpdate(successCallback, errorCallback) {
		self.omsCore.collectionDocUpdate(self.omsCore.config.collections.objects, hookArg.filter, hookArg.updateAttributes, function(error, docDiffs) {
			if(error)
				errorCallback(error);
			else {
				hookArg.docDiffs = docDiffs;
				successCallback();
			}
		});
	}

	function postDocUpdate(successCallback, errorCallback) {
		self._runPluginHooks('postDocUpdate', hookArg, function(error) {
			if(error)
				errorCallback(error);
			else
				successCallback();
		});
	}
};

Oms.prototype.docFindVersion = function(filter, version, callback) {
	var self = this;
	var docReturn = null;

	var hookArg = {
		filter: filter,
		version: version
	};

	Utils.callbackQueue(
		[preDocFindVersion, docFindVersion, postDocFindVersion],
		function(error) {
			callback(error, null);
		},
		function() {
			callback(null, true);
		}
	);

	function preDocFindVersion(successCallback, errorCallback) {
		self._runPluginHooks('preDocFindVersion', hookArg, function(error) {
			if(error)
				errorCallback(error);
			else
				successCallback();
		});
	}

	function docFindVersion(successCallback, errorCallback) {
		self.omsCore.collectionDocFindVersion(self.omsCore.config.collections.objects, hookArg.filter, hookArg.version, function(error, doc) {
			if(error)
				errorCallback(error);
			else {
				hookArg.doc = doc;
				successCallback();
			}
		});
	}

	function postDocFindVersion(successCallback, errorCallback) {
		self._runPluginHooks('postDocFindVersion', hookArg, function(error) {
			if(error)
				errorCallback(error);
			else
				successCallback();
		});
	}
};

Oms.prototype.docFindDateVersion = function(filter, date, callback) {

	var self = this;

	var hookArg = {
		filter: filter,
		date: date
	};

	Utils.callbackQueue(
		[preDocFindDateVersion, docFindDateVersion, postDocFindDateVersion],
		function(error) {
			callback(error, null);
		},
		function() {
			callback(null, true);
		}
	);

	function preDocFindDateVersion(successCallback, errorCallback) {
		self._runPluginHooks('preDocFindDateVersion', hookArg, function(error) {
			if(error)
				errorCallback(error);
			else
				successCallback();
		});
	}

	function docFindDateVersion(successCallback, errorCallback) {
		self.omsCore.collectionDocFindDateVersion(self.omsCore.config.collections.objects, hookArg.filter, hookArg.date, function(error, doc) {
			if(error)
				errorCallback(error);
			else {
				hookArg.doc = doc;
				successCallback();
			}
		});
	}

	function postDocFindDateVersion(successCallback, errorCallback) {
		self._runPluginHooks('postDocFindDateVersion', hookArg, function(error) {
			if(error)
				errorCallback(error);
			else
				successCallback();
		});
	}
};

Oms.prototype.loadPlugins = function(pluginsConfig, oms) {
	if(typeof oms === 'undefined')
		oms = this;
	if(typeof oms.plugins === 'undefined')
		oms.plugins = {};

	Utils.objectForEach(pluginsConfig, function(pluginConfig, pluginKey) {
		oms.plugins[pluginKey] = oms.loadPlugin(pluginConfig, oms);
	});
};

Oms.prototype.loadPlugin = function(pluginConfig, oms) {
	var PluginClass = require(pluginConfig.file);
	return new PluginClass(pluginConfig.config, oms);
};

Oms.prototype._runPluginHooks = function(hookName, hookArg, hooksDone) {
	// Move to init
	var hooks = [];
	Utils.objectForEach(this.plugins, function(plugin) {
		if(typeof plugin.hook === 'function')
			hooks.push(function () {
				plugin.hook.apply(plugin, Array.prototype.slice.call(arguments));
			});
	});

	var self = this;
	var queueReturned = false;
	function hooksErrorSafe(error) {
		if(!queueReturned) {
			queueReturned = true;
			hooksDone(error, null);
		}
	}

	function hooksDoneSafe() {
		if(!queueReturned) {
			queueReturned = true;
			hooksDone.apply(self, [null].concat(Array.prototype.slice.call(arguments)))
		}
	}

	Utils.callbackQueue(hooks, [hookName, hookArg], hooksErrorSafe, hooksDoneSafe, {expand: true});
};

module.exports = Oms;
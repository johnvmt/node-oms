var Utils = require('./Utils');

function OmsPermissions(pluginConfig, oms) {
	this.config = pluginConfig;
	this.oms = oms;
	this._attachGlobalFunctions();
}

OmsPermissions.prototype.docUpdate = function(userIds, updateAttributes, docFilter, callback) {
	var filter = this._permissionFiltersActionMerge(userIds, 'write', docFilter);
	this.oms.docUpdate(updateAttributes, docFilter, callback);
};

OmsPermissions.prototype.docInsert = function(userPermissions, doc, callback) {
	if(typeof userPermissions == 'string') { // passed a userId, apply full permissions
		doc.permissions = {};
		doc.permissions[userPermissions] = {owner: true};
	}
	else
		doc.permissions = userPermissions;

	this.oms.docInsert(doc, callback);
};

OmsPermissions.prototype.docUpdatePermissions = function(userIds, userPermissionsUpdate, docFilter, callback) {
	// overwrite user permissions on docs
	var updateAttributes = {};
	Utils.objectForEach(userPermissionsUpdate, function(permissionsAttribute, permissionsAttributeKey) {
		updateAttributes['permissions.' + permissionsAttributeKey] = permissionsAttribute;
	});

	var filter = this._permissionFiltersActionMerge(userIds, 'permissions', docFilter);
	this.oms.docUpdate({$set: updateAttributes}, filter, callback);
};

OmsPermissions.prototype.docAddPermissions = function(userIds, permissionUserIds, actions, docFilter, callback) {
	// add specified actions for users  on docs
	if(typeof actions == 'string')
		actions = [actions];
	if(typeof permissionUserIds == 'string')
		permissionUserIds = [permissionUserIds];

	if(!Array.isArray(actions))
		callback("actions_invalid", null);
	else if(!Array.isArray(permissionUserIds))
		callback("users_invalid", null);
	else {
		var updateAddToSetAttributes = {};
		permissionUserIds.forEach(function (permissionUserId) {
			updateAddToSetAttributes['permissions.' + permissionUserId + '.actions'] =  {$each: actions};
		});

		var filter = this._permissionFiltersActionMerge(userIds, 'permissions', docFilter);
		this.oms.docUpdate({$addToSet: updateAddToSetAttributes}, filter, callback);
	}
};

OmsPermissions.prototype.docRemovePermissions = function(userIds, permissionUserIds, actions, docFilter, callback) {
	// remove specified actions for users on docs
	if(typeof actions == 'string')
		actions = [actions];
	if(typeof permissionUserIds == 'string')
		permissionUserIds = [permissionUserIds];

	if(!Array.isArray(actions))
		callback("actions_invalid", null);
	else if(!Array.isArray(permissionUserIds))
		callback("users_invalid", null);
	else {
		var updateRemoveAttributes = {};
		permissionUserIds.forEach(function (permissionUserId) {
			updateRemoveAttributes['permissions.' + permissionUserId + '.actions'] =  {$in: actions};
		});

		var filter = this._permissionFiltersActionMerge(userIds, 'permissions', docFilter);
		this.oms.docUpdate({$pull: updateRemoveAttributes}, filter, callback);
	}
};

OmsPermissions.prototype.docFind = function() {
	this.docActionFind.apply(this, [arguments[0], 'read'].concat(Array.prototype.slice.call(arguments, 1)));
};

OmsPermissions.prototype.docActionFind = function() {
	// userIds, action, filters, [fields], callback
	this.oms.docFind.apply(
		this.oms,
		[this._permissionFiltersActionMerge(arguments[0], arguments[1], arguments[2])].concat(Array.prototype.slice.call(arguments, 3))
	);
};

OmsPermissions.prototype._permissionFiltersActionMerge = function(userIds, docAction, docFilter) {
	if(!Array.isArray(userIds))
		userIds = [userIds];

	return {
		$and: [
			this.oms.omsCore.filterValidate(docFilter),
			this._permissionFiltersAction(userIds, docAction)
		]
	};

};

OmsPermissions.prototype._permissionFiltersAction = function(userIds, docAction) {
	var permissionFilters = [
		{'permissions': {$exists: false}}
	];

	userIds.forEach(function(userId) {
		permissionFilters.push(keyValObj('permissions.' + userId + '.owner', true));
		permissionFilters.push(keyValObj('permissions.' + userId + '.actions', {$in: [docAction]}));
	});

	return { $or: permissionFilters };

	function keyValObj(key, val) {
		// create an object with a single attribute
		var obj = {};
		obj[key] = val;
		return obj;
	}
};

OmsPermissions.prototype._attachGlobalFunctions = function() {
	var self = this;

	function globalFunctionName(localFunctionName) {
		return 'permissions' + localFunctionName.charAt(0).toUpperCase() + localFunctionName.slice(1);
	}

	self._globalFunctions().forEach(function(functionName) {
		self.oms[globalFunctionName(functionName)] = function() {
			self[functionName].apply(self, arguments);
		};
	});
};

OmsPermissions.prototype._globalFunctions = function() {
	function objFunctions(object) {
		var functionNames = [];
		for(var attribute in object) {
			if(typeof object[attribute] === 'function')
				functionNames.push(attribute);
		}
		return functionNames;
	}

	return objFunctions(this).filter(function(attribute) {
		return (attribute[0] !== '_' && attribute !== 'globalFunctions');
	});
};

module.exports = OmsPermissions;
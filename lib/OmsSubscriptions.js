var Utils = require('./Utils');

function OmsSubscriptions(pluginConfig, oms) {
	this.config = pluginConfig;
	this.oms = oms;
	this._attachGlobalFunctions();
	this.subscriptions = {};
}

OmsSubscriptions.prototype.hook = function(hookName, args, callbackNext, callbackError) {
	var self = this;
	switch(hookName) {
		case 'preDocUpdate':
			break;
		case 'postDocUpdate':
			if(typeof args.docDiffs == 'object') {
				// Attach update metadata (eg: who made the update)
				var updateMetadata = Utils.objectFilter(args, function(object, key) { return ['updateAttributes', 'filter'].indexOf(key) < 0 });
				Utils.objectForEach(args.docDiffs, function(docDiff, docId) {
					self.trigger(
						docId,
						'update',
						Utils.objectMerge({docDiff: docDiff}, updateMetadata)
					);
				});
			}
			break;
		default: // no hook, continue the queue
			break;
	}
	callbackNext();
};

OmsSubscriptions.prototype._attachGlobalFunctions = function() {
	var self = this;

	function globalFunctionName(localFunctionName) {
		return 'subscription' + localFunctionName.charAt(0).toUpperCase() + localFunctionName.slice(1);
	}

	self._globalFunctions().forEach(function(functionName) {
		self.oms[globalFunctionName(functionName)] = function() {
			self[functionName].apply(self, arguments);
		};
	});
};

OmsSubscriptions.prototype._globalFunctions = function() {
	function objFunctions(object) {
		var functionNames = [];
		for(var attribute in object) {
			if(typeof object[attribute] === 'function')
				functionNames.push(attribute);
		}
		return functionNames;
	}

	return objFunctions(this).filter(function(attribute) {
		return (attribute[0] !== '_' && attribute !== 'globalFunctions');
	});
};


OmsSubscriptions.prototype.add = function(docId, action, subscriptionCallback, callback) {
	var self = this;
	var subscriptionId = Utils.uniqueId();

	if(typeof this.subscriptions[docId] != 'object') {
		this.subscriptions[docId] = {};

		if(typeof this.oms.messengerSubscribe == 'function')
			subscribeMessenger();
		else
			subscribeCallback();
	}
	else
		subscribeCallback();

	if(typeof this.subscriptions[docId][action] != 'object')
		this.subscriptions[docId][action] = {};

	this.subscriptions[docId][action][subscriptionId] = subscriptionCallback;

	function subscribeMessenger() {
		self.oms.messengerSubscribe(docId,
			function(error, message) {
				if(!error)
					self._triggerLocal(message.docId, message.action, message.args);
			},
			function(error) {
				if(error && typeof callback == 'function')
					callback(error, null);
				else
					subscribeCallback();
			}
		);
	}

	function subscribeCallback() {
		if(typeof callback == 'function') {
			callback(null, {
				id: subscriptionId,
				remove: function() {
					self.remove(subscriptionId);
				}
			});
		}
	}
};

OmsSubscriptions.prototype.remove = function(subscriptionId) {
	// Todo implementation that doesn't take O(n^2) to search for subscription
	Utils.objectForEach(this.subscriptions, function(objectSubscriptions) {
		Utils.objectForEach(objectSubscriptions, function(objectActionSubscriptions) {
			if(typeof objectActionSubscriptions[subscriptionId] != 'undefined')
				delete objectActionSubscriptions[subscriptionId];
		});
	});
};

OmsSubscriptions.prototype.trigger = function(docId, action, args) {
	// TODO on doc delete, delete all subscriptions

	if(typeof this.oms.messengerPublish == 'function') {
		this.oms.messengerPublish(docId, {docId: docId, action: action, args: args}, function(error, id) {
			// TODO handle error
			//console.log("TRIGGER", error, id);
		});
	}

	this._triggerLocal(docId, action, args);
};

OmsSubscriptions.prototype._triggerLocal = function(docId, action, args) {
	if(typeof this.subscriptions[docId] == 'object' && typeof this.subscriptions[docId][action] == 'object') {
		Utils.objectForEach(this.subscriptions[docId][action], function(subscriptionCallback) {
			try {
				subscriptionCallback.call(this, args);
			}
			catch(error) {} // TODO delete when callback triggers an error?
		});
	}
};

module.exports = OmsSubscriptions;